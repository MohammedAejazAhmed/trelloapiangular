import {AppInjector} from './app.module';
import {Component} from '@angular/core';
import {HttpClient,HttpHandler} from '@angular/common/http';
import {environment} from './../environments/environment';
import {FetchApisService} from './fetch-apis.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  name: string = environment.boardName;
  listName = environment.listName;
  addingCard: boolean = true;
  listNames: any = [];
  hoverIndex: number = -1;
  pseudo: any = [1];
  currentCardId = '';
  currentCardName = '';
  currentChecklistArray: Array < string > = [];
  checklistOverlay = 0;
  overlay = 0;
  currentChecklistArrayId: Array < string > = [];
  checklistBoxArray: Array < number > = [];
  bigItemArray: Array < Array < string >> = [];
  bigItemArrayId: Array < Array < string >> = [];

  constructor(public fetchApi: FetchApisService) {
    fetchApi.getList().then((resolvedValue) => {
      console.log(resolvedValue);
      this.listNames = resolvedValue;
    })
  }
  onHover(i: number) {
    this.hoverIndex = i;
  }
  addCard() {
    this.addingCard = false;
  }
  closeClick() {
    this.addingCard = true;
  }
  deleteCard(item: string, index: string) {

    for (let iterate = 0; iterate < this.listNames.length; iterate++) {
      if (this.listNames[iterate] === item) {
        this.listNames.splice(iterate, 1);
        this.fetchApi.deleteCard(index)
          .then((data: object) => {
            console.log(data);
          });
        this.fetchApi.listId.splice(this.fetchApi.listId.indexOf(index), 1);
      }
    }
  }
  addTheCard(name: string) {
    console.log(name)
    this.fetchApi.addingCard(name).
    then((data: any) => {
      console.log(data);
      this.listNames.push(name);
      this.fetchApi.getLastCard()
        .then((data: any) => {
          this.fetchApi.listId.push(data['id']);
        })
    });
  }

  async clickCard(cardId: any, name: string) {

    this.currentCardId = cardId;
    this.currentCardName = name;
    this.currentChecklistArray = [];
    this.currentChecklistArrayId = [];
    this.checklistBoxArray = [];
    this.bigItemArrayId = [];
    this.bigItemArray = [];
    this.overlay = 1;
    let data: any = await this.fetchApi.fetchListArray(cardId);
    for (let iterate = 0; iterate < data.length; iterate++) {
      let iterati = iterate.toString();
      await this.getItem(data[iterati]['id']);
      this.currentChecklistArray.push(data[iterati]['name']);
      this.currentChecklistArrayId.push(data[iterati]['id']);
      this.checklistBoxArray.push(0);
    }
    console.log(this.bigItemArray);
  }
  async getItem(data: string) {
    let itemArray: any = await this.fetchApi.fetchItemArray(data)
    let smallItemArray: Array < string > = [];
    let smallItemArrayId: Array < string > = [];
    for (let iterate1 = 0; iterate1 < itemArray.length; iterate1++) {

      let iterati1 = iterate1.toString();
      smallItemArray.push(itemArray[iterati1]['name']);
      smallItemArrayId.push(itemArray[iterati1]['id']);
    }
    this.bigItemArray.push(smallItemArray);
    this.bigItemArrayId.push(smallItemArrayId);
  }

  createChecklist(name: string) {
    //dont forget to add id as well
    this.fetchApi.createChecklist(name, this.currentCardId)
      .then((response) => {
        console.log(response);
        this.fetchApi.lastChecklist(this.currentCardId)
          .then((array: any) => {
            let iterati = (array.length - 1).toString();
            console.log(array[iterati]['id']);
            this.currentChecklistArrayId.push(array[iterati]['id']);
            this.currentChecklistArray.push(name);
            this.checklistBoxArray.push(0);
            let test1: Array<string> = [];
            let test2: Array<string> = [];
            this.bigItemArray.push(test1);
            this.bigItemArrayId.push(test2);
          })
      })
  }
  deleteChecklist(checklistId: string, name: string) {
    this.fetchApi.deleteChecklist(checklistId)
      .then((data) => {
        console.log(data);
        for (let iterate = 0; iterate < this.currentChecklistArray.length; iterate++) {
          if (this.currentChecklistArray[iterate] === name) {
            console.log(name)
            this.currentChecklistArray.splice(iterate, 1);
          }
        }
        for (let iterate = 0; iterate < this.currentChecklistArrayId.length; iterate++) {
          if (this.currentChecklistArrayId[iterate] === checklistId) {
            console.log("yes");
            this.currentChecklistArrayId.splice(iterate, 1);
            this.checklistBoxArray.splice(iterate, 1);
          }
        }
      })
  }
  deleteItem(itemName: string, itemId: string, checklistId: string){
    console.log(itemName);
    console.log(itemId);
    console.log(checklistId);
    this.fetchApi.deletingItem(checklistId, itemId)
    .then((response)=>{
      console.log(response);
      for(let iterate1 = 0; iterate1<this.bigItemArray.length; iterate1++){
        for(let iterate2 = 0; iterate2<this.bigItemArray[iterate1].length; iterate2++){
          if(this.bigItemArray[iterate1][iterate2] === itemName){
            this.bigItemArray[iterate1].splice(iterate2, 1);
            break;
          }
        }
      }
      for(let iterate1 = 0; iterate1<this.bigItemArrayId.length; iterate1++){
        for(let iterate2 = 0; iterate2<this.bigItemArrayId[iterate1].length; iterate2++){
          if(this.bigItemArrayId[iterate1][iterate2] === itemName){
            this.bigItemArrayId[iterate1].splice(iterate2, 1);
            break;
          }
        }
      }
    })
  }
  addItem(itemName: string, checklist: number, checklistId: string){
    console.log(itemName);
    this.fetchApi.addingItem(itemName, checklistId)
    .then((response)=>{
      console.log(response);
      this.fetchApi.fetchItemArray(checklistId)
      .then((response: any)=>{
        let lastId = (response.length-1).toString();
        this.bigItemArray[checklist].push(itemName);
        this.bigItemArrayId[checklist].push(response[lastId]['id']);
      })
    })
  }
}
