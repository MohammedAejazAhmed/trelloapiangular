import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchBoardsService {

  constructor(private http:HttpClient) { }

  fetchBoardId(){
    return new Promise((resolve, reject)=>{
      this.http.get(`https://api.trello.com/1/members/me/boards?key=${environment.key}&token=${environment.token}`)
      .subscribe((response)=>{
        resolve(JSON.parse(JSON.stringify(response)))
      })
    })
  }

}
