import { Component} from '@angular/core';
import { FetchBoardsService } from './fetch-boards.service';
@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent {

  boardNames: Array<string> = [];
  constructor(public fetchBoard: FetchBoardsService) { 
    fetchBoard.fetchBoardId().then((response: any)=>{
      for(let iterate=0; iterate<response.length; iterate++){
        let iterati = iterate.toString();
        this.boardNames.push(response[iterati]['name']);
      }
    })
  } 
}
