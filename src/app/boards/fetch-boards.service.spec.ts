import { TestBed } from '@angular/core/testing';

import { FetchBoardsService } from './fetch-boards.service';

describe('FetchBoardsService', () => {
  let service: FetchBoardsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchBoardsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
