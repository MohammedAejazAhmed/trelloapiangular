import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApisService{

  listArray = '';
  listId :string[] = [];
  constructor(private http: HttpClient) {}

  getList() { 
    return new Promise((resolve, reject) => {
      this.http.get(`https://api.trello.com/1/lists/${environment.listId}/cards?key=${environment.key}&token=${environment.token}`)
        .subscribe((response) => {
          this.listArray = JSON.parse(JSON.stringify(response));
          let listName: string[] = [];
          for (let iterate = 0; iterate < this.listArray.length; iterate++) {
            let list: any = this.listArray[iterate];
            let k: keyof typeof list;
            for (k in list) {
              const v = list[k]; // OK
              if(k === 'name'){
                listName.push(v);
              } else if(k === 'id'){
                this.listId.push(v);
              }
            }
          }
          //console.log(this.listArray[3]);
          resolve(listName);
        })
    })
  }
  getLastCard(){
      return new Promise((resolve, reject)=>{
        this.http.get(`https://api.trello.com/1/lists/${environment.listId}/cards?key=${environment.key}&token=${environment.token}`)
        .subscribe((response)=>{
          let lastList = JSON.parse(JSON.stringify(response));
          for(let iterate = 0; iterate<lastList.length; iterate++){
              if(iterate == lastList.length-1){
                resolve(lastList[iterate]);
              }
          }
        })
      })
  }
  addingCard(name: string): Promise<Response>{
    
    return fetch(`https://api.trello.com/1/cards?key=${environment.key}&token=${environment.token}&idList=${environment.listId}&name=${name}`, {
  method: 'POST'
})
  }
  deleteCard(id: string): Promise<Response>{
    return fetch(`https://api.trello.com/1/cards/${id}?key=${environment.key}&token=${environment.token}`, {
  method: 'DELETE'
    })
  }
  fetchListArray(cardId: string){
    return new Promise((resolve, reject)=>{
      this.http.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${environment.key}&token=${environment.token}`)
      .subscribe((response)=>{
        resolve(JSON.parse(JSON.stringify(response)));
      })
    })
  }
  createChecklist(name: string, id: string): Promise<Response>{
    return fetch(`https://api.trello.com/1/checklists?key=${environment.key}&token=${environment.token}&idCard=${id}&name=${name}`, {
      method: 'POST'
    })
  }
  lastChecklist(card: any){
    return new Promise((resolve, reject)=>{
      this.http.get(`https://api.trello.com/1/cards/${card}/checklists?key=${environment.key}&token=${environment.token}`)
      .subscribe((response)=>{
        resolve(JSON.parse(JSON.stringify(response)));
      })
    })
  }

  deleteChecklist(checkId: string): Promise<Response>{
    return fetch(`https://api.trello.com/1/checklists/${checkId}?&key=${environment.key}&token=${environment.token}`, {
  method: 'DELETE'
    })
  }

  async fetchItemArray(checklistId: string){
    return new Promise((resolve, reject)=>{
      this.http.get(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${environment.key}&token=${environment.token}`)
      .subscribe((response)=>{
        resolve(JSON.parse(JSON.stringify(response)));
      })
    })
  }

  async deletingItem(checklistId:string, itemId:string){
    return fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems/${itemId}?key=${environment.key}&token=${environment.token}`, {
      method: 'DELETE'
    })
  }

  async addingItem(itemName: string, checklistId: string){
    return fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${environment.key}&token=${environment.token}&name=${itemName}`, {
      method: 'POST'
    })
  }
}
