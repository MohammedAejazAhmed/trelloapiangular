import { TestBed } from '@angular/core/testing';

import { FetchApisService } from './fetch-apis.service';

describe('FetchApisService', () => {
  let service: FetchApisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchApisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
